# cutthecord

Modular Client Mod for Discord's Android app.

**Latest supported Discord Android version:** 127.9 - Beta (127109), released on 2022-05-14.

New patch development will be done for the latest supported version.

![A CutTheCord screenshot](https://elixi.re/t/mh3eirsy9.png)

Check out [README.md in patches folder to see what patches are available and what each of them do](patches/README.md)!

## Binaries (apk)

Builds are manually done in the release tab. ~~If you want your own custom build with your own resources, please message me on discord at 46620#4662 to get your own build setup.~~ I'm currently not doing custom builds as I have to do all builds manually, once automation comes back, I will open up to customs. **Rooting is NOT needed, CutTheCord can be installed alongside official Discord and/or other CutTheCord branches.**

Feel free to ignore play protect, it's bullshit.

If you fail recaptcha, [follow this](https://gitdab.com/distok/cutthecord/issues/22#issuecomment-82).

## Building

See [BUILDING.md](BUILDING.md).

## Extras
This repo is just a fork of [this repo](https://gitdab.com/distok/cutthecord) with [this commit](https://gitdab.com/leo60228/cutthecord/commit/a8124a4796ac10e6bb8f34c3294f8d23f1438af0) merged in. When it gets merged I'll remove this notice.

## License

- CTCCI, patchport and other scripts are AGPLv3.
- We chose to not license the patches, and are therefore "All Rights Reserved". However, you're allowed to use it to build your own version of CutTheCord, fork CutTheCord, develop your own patches etc, and we kindly ask you to send us any patches you develop that you think may be helpful. You're free to distribute binaries (apks) including CutTheCord patches as long as you give appropriate credit to the CutTheCord project.
