## DisTok CutTheCord: Better Rotation

This patch changes rotation settings to disallow upside down rotation.

Contributed by [clienthax](https://gitdab.com/clienthax).