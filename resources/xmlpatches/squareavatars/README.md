## DisTok CutTheCord: Square Avatars Patch

This patch turns the circle avatars to their full square versions, letting you see the whole image.

A very small amount of rounding is applied to make it look a little better.
