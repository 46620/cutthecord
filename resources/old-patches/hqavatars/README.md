## DisTok CutTheCord: HQ Avatars

Changes avatars from 128x128 to 256x256 variants.

#### Available and tested on:
- 56.0
- 57.11
- 63.2
- 63.3
- 65.3
- 66.14
- 69.0
- 70.3
- 70.4
- 70.5
- 70.6
- 71.2
- 71.3
- 71.4
- 72.1
- 73.1
- 73.8
- 73.11
- 74.0
- 74.10
- 76.8
- 78.5 - Beta
- 78.7 - Beta
- 78.9 - Beta
- 79.7 - Beta
