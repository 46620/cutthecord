## DisTok CutTheCord: Square Avatars Patch

This patch turns the circle avatars to their full square versions, letting you see the whole image.

A very small amount of rounding is applied to make it look a little better.

#### Available and tested on:
- 56.0
- 57.11
- 63.2
- 63.3
- 65.3
- 66.14
- 69.0
- 70.3
- 70.4
- 70.5
- 70.6
- 71.2
- 71.3
- 71.4
- 72.1
- 73.1
- 73.8
- 73.11
- 74.0
- 74.10
- 76.8
- 78.5 - Beta
- 78.7 - Beta
- 78.9 - Beta
- 79.7 - Beta
- 81.9 - Beta
- 81.10 - Beta
- 81.11 - Beta
- 82.6 - Alpha
- 83.10 - Alpha
- 86.1 - Alpha
- 87.1 - Alpha
- 87.2 - Alpha
- 87.3 - Alpha
- 88.2 - Alpha
- 88.3 - Alpha
- 88.5 - Alpha
- 89.1 - Alpha
- 89.6 - Alpha
- 89.7 - Alpha
