## CutTheCord: Old Patches

This folder contains old patches.

Here's why these patches are no longer maintained in this form:
- hqavatars:     Bitch to maintain, did not want to port to 79107, if fixed will readd to the patch list
- noblocked:     xmlpatch takeover
- squareavatars: xmlpatch takeover
- mutan:         File was changed to a one-liner, I am not going to fix that

The following patches are marked as old because I currently do not feel like porting, they will possibly return in a future update, but currently I can not be fucking asked to port these:
- embedlinks
- nospoiler
- notrack
- nozlib
- showtag
- supplemental