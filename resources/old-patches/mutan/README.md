## DisTok CutTheCord: Mutant Standard Emoji Patch

This patch replaces internal emoji list with the custom emojis of mutant standard.

Custom color modifiers and mutant modifiers (paw, claw) are added too.

Additionally, a feature to generate diverse emojis are added. To use it, put the emoji you want, then write :skin-tone-, pick a skin tone, and remove the space between them (`:ok_hand::skin-tone-2:` etc). When you post that, it'll be sent as a diverse version of that emoji.

You'll need to pack in the right images to the apk. See BUILDING.md at the root of the repo for more information.

Various scripts are provided to help building of custom patches easier.

#### Bugs / Side effects
- Not all emojis are replaced
- Fitzpatrick values are not provided for non-human variants of mutated emojis
- Custom mutstd emojis won't be visible for users who aren't using a patch similar to this.

#### Available and tested on:
- 56.0
- 78.4 - Alpha
- 90.1 - Alpha
- 90.3 - Alpha
- 90.4 - Alpha
- 91.1 - Alpha
- 91.2 - Alpha
- 92.0 - Alpha
- 92.3 - Alpha
- 93.0 - Alpha
- 93.5 - Alpha
- 94.0 - Alpha
- 95.2 - Alpha
- 95.6 - Alpha
- 96.0 - Beta
- 98.0 - Alpha
- 99.1 - Alpha
- 99.10 - Alpha
- 104.4 - Alpha
- 105.2 - Alpha
- 108.1 - Alpha
- 119.5 - Beta
- 94.3 - Alpha
- 94.1 - Alpha

#### Disclaimer

This patch uses Mutant Standard emoji (https://mutant.tech), which are licensed under a Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License (https://creativecommons.org/licenses/by-nc-sa/4.0/).
