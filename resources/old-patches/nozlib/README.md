## DisTok CutTheCord: No zlib-stream Patch

This patch disables zlib-stream field on wss, making it easier to parse it.

#### Available and tested on:
- 89.7 - Alpha
- 90.1 - Alpha
- 90.3 - Alpha
- 90.4 - Alpha
- 91.1 - Alpha
- 91.2 - Alpha
- 92.0 - Alpha
- 92.3 - Alpha
- 93.0 - Alpha
- 93.5 - Alpha
- 94.0 - Alpha
- 95.2 - Alpha
- 95.6 - Alpha
- 96.0 - Beta
- 98.0 - Alpha
- 99.1 - Alpha
- 99.10 - Alpha
- 104.4 - Alpha
- 105.2 - Alpha
- 108.1 - Alpha
- 119.5 - Beta
- 94.3 - Alpha
- 94.1 - Alpha
