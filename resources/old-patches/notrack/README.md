## DisTok CutTheCord: No Track Patch

This patch disables `track` (aka `science`) endpoint, firebase tracking, fabric.io tracking and crashlytics.

As of 8.3.5g, it also removes various fields from `superProperties` that could be used to track your device. This was planned to be added all the way back in 8.3.2, but I apparently forgot to commit it. Bleh. Also as of 8.3.5, adjust and ganalytics are blocked as well. That should be all.

Fuck telemetry.

#### Available and tested on:
- 89.7 - Alpha
- 90.1 - Alpha
- 90.3 - Alpha
- 90.4 - Alpha
- 91.1 - Alpha
- 91.2 - Alpha
- 92.0 - Alpha
- 92.3 - Alpha
- 93.0 - Alpha
- 93.5 - Alpha
- 94.0 - Alpha
- 95.2 - Alpha
- 95.6 - Alpha
- 96.0 - Beta
- 98.0 - Alpha
- 99.1 - Alpha
- 99.10 - Alpha
- 104.4 - Alpha
- 105.2 - Alpha
- 108.1 - Alpha
- 119.5 - Beta
- 94.3 - Alpha
- 94.1 - Alpha
