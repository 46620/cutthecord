\cp res/mipmap-xxxhdpi/logo_debug.png res/mipmap-hdpi/ic_logo.png
\cp res/mipmap-xxxhdpi/logo_debug.png res/mipmap-xhdpi/ic_logo.png
\cp res/mipmap-xxxhdpi/logo_debug.png res/mipmap-xxhdpi/ic_logo.png
\cp res/mipmap-xxxhdpi/logo_debug.png res/mipmap-xxxhdpi/ic_logo.png

\cp res/mipmap-xxxhdpi/logo_debug.png res/mipmap-hdpi/ic_logo_round.png
\cp res/mipmap-xxxhdpi/logo_debug.png res/mipmap-xhdpi/ic_logo_round.png
\cp res/mipmap-xxxhdpi/logo_debug.png res/mipmap-xxhdpi/ic_logo_round.png
\cp res/mipmap-xxxhdpi/logo_debug.png res/mipmap-xxxhdpi/ic_logo_round.png

# It stopped working on 1505 so I added a lot of extra stuff

\cp res/mipmap-xxxhdpi/logo_debug.png res/mipmap-hdpi/ic_launcher_foreground.png
\cp res/mipmap-xxxhdpi/logo_debug.png res/mipmap-xhdpi/ic_launcher_foreground.png
\cp res/mipmap-xxxhdpi/logo_debug.png res/mipmap-xxhdpi/ic_launcher_foreground.png
\cp res/mipmap-xxxhdpi/logo_debug.png res/mipmap-xxxhdpi/ic_launcher_foreground.png

# Another fix for 76008, I plan to clean this up later and not make it gross

\cp res/mipmap-xxxhdpi/logo_debug.png res/mipmap-hdpi/ic_logo_foreground.png
\cp res/mipmap-xxxhdpi/logo_debug.png res/mipmap-xhdpi/ic_logo_foreground.png
\cp res/mipmap-xxxhdpi/logo_debug.png res/mipmap-xxhdpi/ic_logo_foreground.png
\cp res/mipmap-xxxhdpi/logo_debug.png res/mipmap-xxxhdpi/ic_logo_foreground.png

#rm -rf res/mipmap-anydpi-v26/
