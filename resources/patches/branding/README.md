## DisTok CutTheCord: Custom Branding Patch

This patch renames Discord app to CutTheCord.

It also enables debuggability (`android:debuggable`) and backupability (`android:allowBackup`) of the application.

You'll need to run "addpatch.py" with syntax of `python3 addpatch.py $in_patch_filename $app_name $branch_name` and use the `-custom.patch` version, or else it'll fail. You'll need to put a valid directory for storing hacky build counters to it.

Running `customicon.sh` (while on root of the extracted folder) will also change the icon to the debug icon:

![](https://elixi.re/i/48nc2je7.png)

If you want a different icon, replace `res/mipmap-xxxhdpi/logo_debug.png` with the icon you want before running `customicon.sh`. If you don't want dynamic icon to be shown instead of the custom icon, delete the folder `res/mipmap-anydpi-v26/` (or if you want to replace it too, look at the next line instead).

If you want a custom dynamic icon replace `res/mipmap-xxxhdpi/ic_launcher_foreground.png` (optionally transparent image with only the foreground of the icon) and `res/mipmap-xxxhdpi/ic_launcher_background.png` (optionally transparent image with only the background of the icon), both images should be square and of same size, and then run `customdynamicicon.sh`.

You can use the following line to patch authorities:

`sed -i 's/android:authorities="com.discord./android:authorities="com.cutthecord.CTCBRANCH./g' AndroidManifest.xml`

#### Available and tested on:
- 56.0
- 57.11
- 63.2
- 63.3
- 65.3
- 66.14
- 69.0
- 70.3
- 70.4
- 70.5
- 70.6
- 71.2
- 71.3
- 71.4
- 72.1
- 73.1
- 73.8
- 73.11
- 74.0
- 74.10
- 76.8
- 78.5 - Beta
- 78.7 - Beta
- 78.9 - Beta
- 79.7 - Beta
- 81.9 - Beta
- 81.10 - Beta
- 81.11 - Beta
- 82.6 - Alpha
- 83.10 - Alpha
- 86.1 - Alpha
- 87.1 - Alpha
- 87.2 - Alpha
- 87.3 - Alpha
- 88.2 - Alpha
- 88.3 - Alpha
- 88.5 - Alpha
- 89.1 - Alpha
- 89.6 - Alpha
- 89.7 - Alpha
- 90.1 - Alpha
- 90.3 - Alpha
- 90.4 - Alpha
- 91.1 - Alpha
- 91.2 - Alpha
- 92.0 - Alpha
- 92.3 - Alpha
- 93.0 - Alpha
- 93.5 - Alpha
- 94.0 - Alpha
- 95.2 - Alpha
- 95.6 - Alpha
- 96.0 - Beta
- 98.0 - Alpha
- 99.1 - Alpha
- 99.10 - Alpha
- 104.4 - Alpha
- 105.2 - Alpha
- 108.1 - Alpha
- 119.5 - Beta
- 94.3 - Alpha
- 94.1 - Alpha
- 127.9 - Beta
