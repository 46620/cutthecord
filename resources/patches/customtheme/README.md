## DisTok CutTheCord: Custom Theme Patch

This patch replaces theme with a pitch black theme, similar to [0cord](https://gitlab.com/ao/0cord). However, you're free to design your own theme. A reference for the values you need are provided below.

Run `fixsplash.sh <full path of asset_loading.png>` to fix the splash.

Self note: Top bar color is set by `setStatusBarColorResourceId`

#### Bugs / Side effects
- Not all colors are replaced (almost all are, though)
- Status bar color is only changed on chat view (not on settings etc)

#### Available and tested on:
- 89.7 - Alpha
- 90.1 - Alpha
- 90.3 - Alpha
- 90.4 - Alpha
- 91.1 - Alpha
- 91.2 - Alpha
- 92.0 - Alpha
- 92.3 - Alpha
- 93.0 - Alpha
- 93.5 - Alpha
- 94.0 - Alpha
- 95.2 - Alpha
- 95.6 - Alpha
- 96.0 - Beta
- 98.0 - Alpha
- 99.1 - Alpha
- 99.10 - Alpha
- 104.4 - Alpha
- 105.2 - Alpha
- 108.1 - Alpha
- 119.5 - Beta
- 94.3 - Alpha
- 94.1 - Alpha
- 127.9 - Beta


#### Value reference

These are mostly outdated.

- primary_100 -> button/title color (Friends, User Settings, your name on bottom on sidebar, channel name)
- primary_300 -> entry color (names on direct messages), also used on dtag on small user profile
- primary_400 -> label color (Direct Messages, "online", your discrim on bottom of sidebar, note field, @/# on channel name bar)
- primary_500 -> "Create Instant Invite"
- primary_500_alpha_60 -> Used for muted channels
- primary_600 -> chat background, also used by cutthecord for status bar
- primary_630 -> sidebar color, bottom bar color (not chat), small user profile bg color, secondary color on many menus like settings
- grey_guilds -> guild bar
- purple_brand -> blurple, used for things like bottom circle button for message on someone's profile, bot icon, mentions
- dark_grey_2 -> bottom chat bar
- grey_account -> user sidebar background (user list)
- grey_channels -> user sidebar background (channel information)
- link_100-900 -> Link colors (pick a color, put to 500, fill in rest with 0to255.com)
