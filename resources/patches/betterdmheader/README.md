## DisTok CutTheCord: Better DM Header Patch

This patch replaces the call button on DM headers with search button. The call button is still available in the right menu.

As of 89207, the patch is now split into an xml and smali patch

![](https://lasagna.cat/i/9njfeecd.png)

#### Available and tested on:
- 56.0
- 57.11
- 63.2
- 63.3
- 65.3
- 66.14
- 69.0
- 70.3
- 70.4
- 70.5
- 70.6
- 71.2
- 71.3
- 71.4
- 72.1
- 73.1
- 73.8
- 73.11
- 74.0
- 74.10
- 76.8
- 78.5 - Beta
- 78.7 - Beta
- 78.9 - Beta
- 79.7 - Beta
- 81.9 - Beta
- 81.10 - Beta
- 81.11 - Beta
- 82.6 - Alpha
- 83.10 - Alpha
- 86.1 - Alpha
- 87.1 - Alpha
- 87.2 - Alpha
- 87.3 - Alpha
- 88.2 - Alpha
- 88.3 - Alpha
- 88.5 - Alpha
- 89.1 - Alpha
- 89.6 - Alpha
- 89.7 - Alpha
- 90.1 - Alpha
- 90.3 - Alpha
- 90.4 - Alpha
- 91.1 - Alpha
- 91.2 - Alpha
- 92.0 - Alpha
- 92.3 - Alpha
- 93.0 - Alpha
- 93.5 - Alpha
- 94.0 - Alpha
- 95.2 - Alpha
- 95.6 - Alpha
- 96.0 - Beta
- 98.0 - Alpha
- 99.1 - Alpha
- 99.10 - Alpha
- 104.4 - Alpha
- 105.2 - Alpha
- 108.1 - Alpha
- 119.5 - Beta
- 94.3 - Alpha
- 94.1 - Alpha
- 127.9 - Beta
